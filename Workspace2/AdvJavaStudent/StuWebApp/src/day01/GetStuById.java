package day01;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.StudentDAO;
import com.dto.Student;

@WebServlet("/GetStuById")
public class GetStuById extends HttpServlet {

protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		int stuId = Integer.parseInt(request.getParameter("stuId"));

		StudentDAO stuDao = new StudentDAO();
		Student stu = stuDao.getStudentById(stuId);

		if (stu != null) {

			//Store the stu data under request object
			request.setAttribute("stu", stu);
			
			RequestDispatcher rd = request.getRequestDispatcher("GetStudentById.jsp");
			rd.forward(request, response);

		} else {
			RequestDispatcher rd = request.getRequestDispatcher("AdminHomePage.jsp");
			rd.include(request, response);
			
			out.println("<center>");
			out.println("<h1 style='color:red;'>Student Record Not Found!!!</h1>");	
			out.println("</center>");
		}
		
	}




	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}