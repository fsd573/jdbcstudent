package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.db.DbConnection;
import com.dto.Student;

public class StudentDAO {
	
public Student stuLogin(String emailId, String password) {
		
		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		String loginQuery = "Select * from student where emailId=? and password=?";
		
		
		try {
			pst = con.prepareStatement(loginQuery);
			pst.setString(1, emailId);
			pst.setString(2, password);
			rs = pst.executeQuery();
			
			if (rs.next()) {
				Student stu = new Student();
				stu.setStuId(rs.getInt(1));
				stu.setStuName(rs.getString(2));
				stu.setGender(rs.getString(3));
				stu.setEmailId(rs.getString(4));
				stu.setPassword(rs.getString(5));
				stu.setCourse(rs.getString(6));
				return stu;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			if (con != null) {
				try {
					rs.close();
					pst.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		
		return null;
	}

public List<Student> getAllStudents() {
	
	Connection con = DbConnection.getConnection();
	PreparedStatement pst = null;
	ResultSet rs = null;		
	List<Student> stuList = null;
	
	String selectQuery = "Select * from student";
	
	
	try {
		pst = con.prepareStatement(selectQuery);
		rs = pst.executeQuery();
		
		stuList = new ArrayList<Student>();
		
		while (rs.next()) {
			Student stu = new Student();
			
			stu.setStuId(rs.getInt(1));
			stu.setStuName(rs.getString(2));
			stu.setGender(rs.getString(3));
			stu.setEmailId(rs.getString(4));
			stu.setPassword(rs.getString(5));
			stu.setCourse(rs.getString(6));
			
			stuList.add(stu);
		}
		
	} catch (SQLException e) {
		e.printStackTrace();
	}
	
	finally {
		if (con != null) {
			try {
				rs.close();
				pst.close();
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	return stuList;
}

public int registerStudent(Student stu) {
	Connection con = DbConnection.getConnection();
	PreparedStatement pst = null;
	
	String insertQuery = "insert into student " + 
	"(stuName, gender, emailId, password, course) values (?, ?, ?, ?, ?)";
	
	try {
		pst = con.prepareStatement(insertQuery);
		
		pst.setString(1, stu.getStuName());
		pst.setString(2, stu.getGender());
		pst.setString(3, stu.getEmailId());
		pst.setString(4, stu.getPassword());
		pst.setString(5, stu.getCourse());
		
		return pst.executeUpdate();
		
	} catch (SQLException e) {
		e.printStackTrace();
	}
	
	finally {
		if (con != null) {
			try {
				pst.close();
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	return 0;
}

public Student getStudentById(int stuId) {	
	
	Connection con = DbConnection.getConnection();
	PreparedStatement pst = null;
	ResultSet rs = null;
	
	String selectQuery = "Select * from student where stuId=?";
	
	
	try {
		pst = con.prepareStatement(selectQuery);
		pst.setInt(1, stuId);
		rs = pst.executeQuery();
		
		if (rs.next()) {
			
			Student stu = new Student();
			
			stu.setStuId(rs.getInt(1));
			stu.setStuName(rs.getString(2));
			stu.setGender(rs.getString(3));
			stu.setEmailId(rs.getString(4));
			stu.setPassword(rs.getString(5));
			stu.setCourse(rs.getString(6));
			
			return stu;
		}
		
	} catch (SQLException e) {
		e.printStackTrace();
	}
	
	finally {
		if (con != null) {
			try {
				rs.close();
				pst.close();
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	return null;
}

public int deleteStudent(int stuId) {
	Connection con = DbConnection.getConnection();
	PreparedStatement pst = null;
	
	String deleteQuery = "delete from student where stuId=?";
	
	
	try {
		pst = con.prepareStatement(deleteQuery);
		pst.setInt(1, stuId);
		
		return pst.executeUpdate();
		
	} catch (SQLException e) {
		e.printStackTrace();
	}
	
	finally {
		if (con != null) {
			try {
				pst.close();
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	return 0;
}

public int updateStudent(Student stu) {
	Connection con = DbConnection.getConnection();
	PreparedStatement pst = null;
	
	String updateQuery = "update student set stuName=?, gender=?, emailId=?, password=?, course=? where stuId=?";
	
	try {
		pst = con.prepareStatement(updateQuery);
		
		pst.setString(1, stu.getStuName());
		pst.setString(2, stu.getGender());
		pst.setString(3, stu.getEmailId());
		pst.setString(4, stu.getPassword());
		pst.setString(5, stu.getCourse());
		pst.setInt(6, stu.getStuId());
		
		return pst.executeUpdate();
		
	} catch (SQLException e) {
		e.printStackTrace();
	}
	
	finally {
		if (con != null) {
			try {
				pst.close();
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	return 0;
}




}

